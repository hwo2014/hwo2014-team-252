package superbot;

import util.CarPosition;
import util.GameInit;

public class Bot {
	
	public void changeGameTick(Integer gameTick){
    	if (gameTick != null){
    		previousGameTick = this.gameTick;
    		this.gameTick = gameTick;
    		deltaGameTick = gameTick - previousGameTick;
    	}
	}
	
    public void changeAngle(CarPosition carPosition) {
    	previousAngle = newAngle;
    	newAngle =  carPosition.angle;    	    	
    	previousDeltaAngle = deltaAngle;
    	if (gameTick != 0){
    		deltaAngle = (newAngle - previousAngle) / deltaGameTick;    		
    	}
	}


	
	public void changeSpeed(CarPosition carPosition) {
    	previousPieceDistance = currentPieceDistance;
    	previousPieceIndex = currentPieceIndex;
     	currentPieceDistance = carPosition.piecePosition.inPieceDistance;
     	currentPieceIndex = carPosition.piecePosition.pieceIndex;
     	
     	if (previousPieceIndex == null){
     		return ;
     	}
     	previousSpeed = currentSpeed;
    	if (previousPieceIndex == currentPieceIndex){
    		currentSpeed = (currentPieceDistance - previousPieceDistance) / deltaGameTick;    		
    	} else {
    		double deltaS = gameInit.race.track.pieces[previousPieceIndex].getLength() - previousPieceDistance + currentPieceDistance;
    		currentSpeed = (float) (deltaS / deltaGameTick);
    	}
    	if (currentSpeed < -1){
    		// TODO muito mal informado!
    		currentSpeed =  previousSpeed;
    	}
		
	}
	
    public void updateBotInfo(CarPosition carPosition, Integer gameTick) {
    	changeGameTick(gameTick);
    	changeAngle(carPosition);
    	changeSpeed(carPosition);
    	printStatus();
	}


	protected void printStatus() {
    	System.out.printf("speed: %2.2f; newAngle: %2.2f; deltaAngle: %2.2f; throttle: %2.2f\n ", currentSpeed, newAngle, deltaAngle, throttle);                	
	}

	private float previousAngle = 0;
    protected float newAngle = 0;
    protected float deltaAngle;
    private float previousDeltaAngle;
    private float deltaDeltaAngle;
    
    private float previousPosition;
    private float position;
    private int gameTick = 0;
    private int previousGameTick;
    private int deltaGameTick;
    
    

	protected double throttle = 1;
	protected GameInit gameInit;
	protected float currentSpeed;
	protected Float currentPieceDistance;
	protected Integer currentPieceIndex;
	protected Float previousPieceDistance;
	protected Integer previousPieceIndex;
	protected float previousSpeed;
	protected static String botName;

}
