package superbot;

import com.google.gson.Gson;
import util.CarPosition;
import util.CarPositionMsg;
import util.GameInit;

import java.io.*;
import java.net.Socket;
import java.time.Duration;
import java.time.LocalDateTime;

public class Main extends Bot{

    private String botName;

    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];
        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
        new Main(reader, writer, new Join(botName, botKey));
    }
    

    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        this.botName = join.name;
        String line = null;
        final LocalDateTime start = LocalDateTime.now();
        System.out.println("Started: " + start);
        send(join);
        final Gson gson = new Gson();

        // param
        boolean log = false;
        double s = 0.1;
        double w1 = 1.4;
        double w2 = 1.2;
        double w3 = 0.7;
        double wb = 1.;
        double crash_factor = 1.5;

        // stat
        double kmax = 0.;
        double ksum = 0.;
        double ticks = 0;
        double tsum = 0.;
        double tmin = 1.;
        int crashes = 0;

        GameInit gameInit = null;
        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
            	final CarPositionMsg carPositionMsg = gson.fromJson(line, CarPositionMsg.class);
            	final CarPosition mypos = carPositionMsg.getCarPostions().get(botName);

            	updateBotInfo(mypos, msgFromServer.gameTick);

                if (msgFromServer.gameTick == null){
                    System.out.println("null tick");
                    continue;
                }
                final int pi = mypos.piecePosition.pieceIndex;

                if (log) {
                    System.out.println("tick=" + ticks);
                    System.out.println("idx=" + pi + ",kmax=" + kmax + ", k_avg=" +
                            (ksum / ticks) + ", tmin=" + tmin + ", t_avg=" + (tsum / ticks) + ".");
                }

                final double a1 = getNextAngles(pi, gameInit.race.track.pieces, 1) * s;
                final double a2 = getNextAngles(pi, gameInit.race.track.pieces, 2) * s;
                final double a3 = getNextAngles(pi, gameInit.race.track.pieces, 3) * s;
                final double last = getBeforeAngles(pi, gameInit.race.track.pieces, 3) * s;
                // the bigger k, the slower will be t
                // substract value from before angels to accelerate more after curves

                final double b = (last == 0 && a3 > 0)? 1. : 0.;
                final double k = ( (a1 * w1)  + (a2 * w2) + (a3 * w3) );
                ksum += k;
                if (k > kmax) kmax = k;
                final double x = k < 0? 0 : k;
                final double t = b > 0? 0 : (1 / (1 + x));

                if (t == 0) System.out.println("Break in " + pi);

                tsum += t;
                if (t < tmin) tmin = t;
                if (log) {
                    System.out.println("a1=" + a1 + ", a2=" + a2 + ", a3=" + a3 + ", b=" + b + ", k=" + k + ",x=" + x + "=> t=" + t);
                }
                send(new Throttle(t));
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                System.out.println("Race init");
                gameInit = gson.fromJson(msgFromServer.data.toString(), GameInit.class);
                System.out.println("num cars=" + gameInit.race.cars.length);
                System.out.println("race=" + gameInit.race);
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
            } else if (msgFromServer.msgType.equals("turboAvailable")) {
                System.out.println("turboAvailable");
            } else if (msgFromServer.msgType.equals("crash")) {
                System.out.println("CRASH!!!");
                crashes++;
                s = s * crash_factor;
                for (int i = 0; i < gameInit.race.track.pieces.length; ++i) {
                    System.out.println(i + "." + gameInit.race.track.pieces[i]);
                }
                // see data.name for crashing driver
            } else {
                System.out.println("ping");
                send(new Ping());
            }
        }

        final LocalDateTime end = LocalDateTime.now();
        final Duration d = Duration.between(start, end);
        System.out.println("Ended: " + end);
        System.out.println("Crashes: " + crashes);
        System.out.println("Duration: " + d.getSeconds() + " seconds.");
    }

    private double getNextAngles(int pi, GameInit.Piece[] pieces, int num) {
        int j = pi;
        double a = 0.;
        for (int i = 0; i < num; i++) {
            if (j == pieces.length) j = 0;
            if (pieces[j].angle != null) {
                double absAngle = Math.abs(pieces[j].angle);
                if (absAngle < 1) {
                    // leave 0
                }
                else if (absAngle < 30) {
                    a += 0.5;
                }
                else {
                    a += 1.0;
                }
            }
            ++j;
        }
        return a;
    }

    private double getBeforeAngles(int pi, GameInit.Piece[] pieces, int num) {
        int j = pi;
        double a = 0.;
        for (int i = 0; i < num; i++) {
            if (j < 0) j = pieces.length - 1;
            if (pieces[j].angle != null) {
                double absAngle = Math.abs(pieces[j].angle);
                if (absAngle < 1) {
                    // leave 0
                }
                else if (absAngle < 30) {
                    a += 0.5;
                }
                else {
                    a += 1.0;
                }
            }
            j--;
        }
        return a;
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}


class MsgWrapper {
    public final String msgType;
    public final Object data;
    public Integer gameTick;

    MsgWrapper(final String msgType, final Object data, final Integer gameTick) {
        this.msgType = msgType;
        this.data = data;
        this.gameTick = gameTick;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData(), null);
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}
