package noobbot;

public class Race {
	Track track;

	class Track {
		String id;
		String name;
	}

	class Piece {
		Float length;
		Boolean switcher;
		Integer radius;
		Float angle;
	}

	class Lane {
		Integer distanceFromCenter;
		Integer index;
	}

	class StartingPoint {
		Float angle;
		Position position;
	}

	class Position {
		Float x, y;
	}
}
