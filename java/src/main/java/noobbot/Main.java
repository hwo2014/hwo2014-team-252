package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import superbot.Bot;
import util.CarPosition;
import util.CarPositionMsg;
import util.GameInit;
import util.GameInit.Piece;

import com.google.gson.Gson;

public class Main extends Bot{
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
        
        new Main(reader, writer, new Join(botName, botKey));
        
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;

        send(join);

        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            
            
            
            if (msgFromServer.msgType.equals("carPositions")) {
            	System.out.println(line);
                CarPositionMsg carPositionMsg = gson.fromJson(line, CarPositionMsg.class);
                CarPosition carPosition = carPositionMsg.getCarPostions().get(botName);
                if (msgFromServer.gameTick != null){
                	updateBotInfo(carPosition, msgFromServer.gameTick);

                	shouldBreak(carPosition, currentSpeed);
                	if (Math.abs(deltaAngle) > 0.5){
                		if (Math.signum(deltaAngle) *  Math.signum(newAngle) > 0){
                			speedDown(Math.abs(deltaAngle));            			
                		}
                	}
                }
                send(new Throttle(throttle));
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                gameInit = gson.fromJson(msgFromServer.data.toString(), GameInit.class);            	
                System.out.println("Race init");
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
                System.out.println(line);
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
            } else {
                send(new Ping());
            }
        }
    }

	private void shouldBreak(CarPosition carPosition, float currentSpeed2) {
    	Integer indexCurrentPiece = carPosition.piecePosition.pieceIndex;
    	Piece currentPiece = gameInit.race.track.pieces[indexCurrentPiece];
    	Piece nextPiece = gameInit.race.track.pieces[indexCurrentPiece + 1];
    	// TODO sou forçado a colocar 100... porque o lenght está errado
    	shouldBreak(currentSpeed, carPosition.angle, 100 - carPosition.piecePosition.inPieceDistance, nextPiece);
	}

	private void shouldBreak(float speed, Float angle, float f,
			Piece nextPiece) {
		if (nextPiece.isBend()){
			//  agora que é a conta doida
			// precisa considerar a inclinação da pista
			
			
		}
	}

	private void speedUp() {
    	if (throttle < 0.1){
    		throttle = 0.2;
    	}
    	throttle *= 1.2;
    	if (throttle >= 1){
    		throttle = 1;
    	}
	}

	private void speedDown(float deltaAngle) {
		throttle *= 0.9;
		if (deltaAngle > 3){
			throttle = 0.001;
		}
	}

	private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;
    public Integer gameTick;

    MsgWrapper(final String msgType, final Object data, final Integer gameTick) {
        this.msgType = msgType;
        this.data = data;
        this.gameTick = gameTick;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData(), null);
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}