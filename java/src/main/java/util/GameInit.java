package util;

import com.google.gson.annotations.SerializedName;

import java.util.Arrays;

public class GameInit {

	public final Race race;

	public GameInit(Race race) {
		this.race = race;
	}

	@Override
	public String toString() {
		return "GameInit{" + "race=" + race + '}';
	}

	public static class CarId {
		public final String name;
		public final String color;

		public CarId(String name, String color) {
			this.name = name;
			this.color = color;
		}

		@Override
		public String toString() {
			return "CarId{" + "name='" + name + '\'' + ", color='" + color
					+ '\'' + '}';
		}
	}

	public static class CarDimensions {
		public final Double length;
		public final Double width;
		public final Double guideFlagPosition;

		public CarDimensions(Double length, Double width,
				Double guideFlagPosition) {
			this.length = length;
			this.width = width;
			this.guideFlagPosition = guideFlagPosition;
		}

		@Override
		public String toString() {
			return "CarDimensions{" + "length=" + length + ", width=" + width
					+ ", guideFlagPosition=" + guideFlagPosition + '}';
		}
	}

	public static class Car {
		public final CarId id;
		public final CarDimensions dimensions;

		public Car(CarId id, CarDimensions dimensions) {
			this.id = id;
			this.dimensions = dimensions;
		}

		@Override
		public String toString() {
			return "Car{" + "id=" + id + ", dimensions=" + dimensions + '}';
		}
	}

	public static class Piece {
		public Double length;
		@SerializedName("switch")
		public final Boolean switchFlag;
		public final Double radius;
		public final Double angle;

		public Piece(Double length, Boolean switchFlag, Double radius,
				double angle) {
			this.length = length;
			this.switchFlag = switchFlag;
			this.radius = radius;
			this.angle = angle;
		}

		@Override
		public String toString() {
			return "Piece{" + "length=" + length + ", switchFlag=" + switchFlag
					+ ", radius=" + radius + ", angle=" + angle + '}';
		}
		
		public boolean isBend(){
			return angle != null && angle != 0;
		}
		
		public double getLength() {
			if (length == null){
				length = Math.abs(radius * (angle / 360) * Math.PI * 2); 
			}
			return length;
		}
	}

	public static class Lane {
		public final double distanceFromCenter;
		public final int idx;

		public Lane(double distanceFromCenter, int idx) {
			this.distanceFromCenter = distanceFromCenter;
			this.idx = idx;
		}

		@Override
		public String toString() {
			return "Lane{" + "distanceFromCenter=" + distanceFromCenter
					+ ", idx=" + idx + '}';
		}
	}

	public static class Track {
		public final String id;
		public final String name;
		public final Piece[] pieces;
		public final Lane[] lanes;

		public Track(String id, String name, Piece[] pieces, Lane[] lanes) {
			this.id = id;
			this.name = name;
			this.pieces = pieces;
			this.lanes = lanes;
		}

		@Override
		public String toString() {
			return "Track{" + "id='" + id + '\'' + ", name='" + name + '\''
					+ ", pieces=" + Arrays.toString(pieces) + ", lanes="
					+ Arrays.toString(lanes) + '}';
		}
	}

	public static class Race {
		public final Track track;
		public final Car[] cars;
		public final RaceSession raceSession;

		public Race(Track track, Car[] cars, RaceSession raceSession) {
			this.track = track;
			this.cars = cars;
			this.raceSession = raceSession;
		}

		@Override
		public String toString() {
			return "Race{" + "track=" + track + ", cars="
					+ Arrays.toString(cars) + ", raceSession=" + raceSession
					+ '}';
		}
	}

	public static class RaceSession {
		public final int laps;
		public final long maxLapTimeMs;
		public final boolean quickRace;

		public RaceSession(int laps, long maxLapTimeMs, boolean quickRace) {
			this.laps = laps;
			this.maxLapTimeMs = maxLapTimeMs;
			this.quickRace = quickRace;
		}

		@Override
		public String toString() {
			return "RaceSession{" + "laps=" + laps + ", maxLapTimeMs="
					+ maxLapTimeMs + ", quickRace=" + quickRace + '}';
		}
	}

}
