package util;

/**
 * </pre> The carPositions message describes the position of each car on the
 * track. The piecePosition object consists of
 * 
 * pieceIndex - zero based index of the piece the car is on inPieceDistance -
 * the distance the car's Guide Flag (see above) has traveled from the start of
 * the piece along the current lane lane - a pair of lane indices. Usually
 * startLaneIndex and endLaneIndex are equal, but they do differ when the car is
 * currently switching lane lap - the number of laps the car has completed. The
 * number 0 indicates that the car is on its first lap. The number -1 indicates
 * that it has not yet crossed the start line to begin it's first lap.
 * piece-position
 * 
 * The angle depicts the car's slip angle. Normally this is zero, but when you
 * go to a bend fast enough, the car's tail will start to drift. Naturally,
 * there are limits to how much you can drift without crashing out of the track.
 * 
 * slip-angle
 * 
 * The gameTick attribute indicates the current Server Tick, which is an
 * increasing integer.
 * 
 * The carPositions message will be sent repeatedly on each Server Tick. </pre>
 * 
 * 
 * @author Rodrigo
 *
 */
public class CarPosition {
	
	public CarId id;

	public Float angle;
	
	public PiecePosition piecePosition;

	public Integer lap;

	public static class CarId {
		public String name;
		public String color;
	}

	public static class PiecePosition {
		public Integer pieceIndex;
		public Float inPieceDistance;
		public Lane lane; 
	}

	public static class Lane {
		public Integer startLaneIndex;
		public Integer endLaneIndex;
	}

}
