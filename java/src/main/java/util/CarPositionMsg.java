package util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CarPositionMsg {
	
	String msgType;
	List<CarPosition> data;
	private Map<String, CarPosition> carPositions;
    public final String gameId;
    public final Long gameTick;

    public CarPositionMsg(String gameId, Long gameTick) {
        this.gameId = gameId;
        this.gameTick = gameTick;
    }

    public Map<String, CarPosition> getCarPostions() {
		if (carPositions == null && data != null){
			carPositions = new HashMap<>();
			for (CarPosition carPosition: data) {
				carPositions.put(carPosition.id.name, carPosition);
			}
		}
		return carPositions;
	}

}
